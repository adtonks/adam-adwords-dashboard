CREATE EXTERNAL TABLE `adwords_budget_stats`(
  `associatedcampaignid` bigint,
  `budgetid` bigint,
  `allconversionrate` double,
  `allconversionvalue` double,
  `allconversions` double,
  `associatedcampaignname` string,
  `associatedcampaignstatus` string,
  `averagecost` bigint,
  `averagecpc` bigint,
  `averagecpe` double,
  `averagecpm` bigint,
  `averagecpv` double,
  `averageposition` double,
  `budgetcampaignassociationstatus` string,
  `clicks` bigint,
  `conversionrate` double,
  `conversionvalue` double,
  `conversions` double,
  `cost` bigint,
  `costperallconversion` bigint,
  `costperconversion` bigint,
  `crossdeviceconversions` double,
  `ctr` double,
  `engagementrate` double,
  `engagements` bigint,
  `impressions` bigint,
  `interactionrate` double,
  `interactiontypes` string,
  `interactions` bigint,
  `valueperallconversion` double,
  `valueperconversion` double,
  `videoviewrate` double,
  `videoviews` bigint,
  `viewthroughconversions` bigint)
PARTITIONED BY (
  `externalcustomerid` bigint)
ROW FORMAT SERDE
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
STORED AS INPUTFORMAT
  'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://accuen-test/adam_test/adam-adwords-dashboard/budget-stats'
TBLPROPERTIES (
  'has_encrypted_data'='true',
  'transient_lastDdlTime'='1548955633')
