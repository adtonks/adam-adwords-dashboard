import s3fs
import gc

import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq

access_key = input('Enter AWS key:')
secret = input('Enter AWS secret:')

file_path = {
    'campaign-basic-stats':
        'tables_test/adwords-tables-test_campaign-basic-stats.csv',
    'budget-stats':
        'tables_test/adwords-tables-test_budget-stats.csv',
    'budget': 'tables_test/adwords-tables-test_budget.csv'
}

# types of columns from csv file
# assuming that no values have been nulled
cols_names = {
    'campaign-basic-stats':
        ['basecampaignid', 'campaigngroupid', 'campaignid',
         'externalcustomerid', 'activeviewimpressions',
         'activeviewmeasurability', 'activeviewmeasurablecost',
         'activeviewmeasurableimpressions', 'activeviewviewability',
         'adnetworktype1', 'adnetworktype2', 'averageposition', 'clicks',
         'conversionvalue', 'conversions', 'cost', 'date', 'device',
         'impressions', 'interactiontypes', 'interactions', 'slot',
         'viewthroughconversions'],
    'budget-stats':
        ['associatedcampaignid', 'budgetid', 'externalcustomerid',
         'allconversionrate', 'allconversionvalue', 'allconversions',
         'associatedcampaignname', 'associatedcampaignstatus', 'averagecost',
         'averagecpc', 'averagecpe', 'averagecpm', 'averagecpv',
         'averageposition', 'budgetcampaignassociationstatus', 'clicks',
         'conversionrate', 'conversionvalue', 'conversions', 'cost',
         'costperallconversion', 'costperconversion', 'crossdeviceconversions',
         'ctr', 'engagementrate', 'engagements', 'impressions',
         'interactionrate', 'interactiontypes', 'interactions',
         'valueperallconversion', 'valueperconversion', 'videoviewrate',
         'videoviews', 'viewthroughconversions'],
    'budget':
        ['budgetid', 'externalcustomerid', 'accountdescriptivename', 'amount',
         'budgetname', 'budgetreferencecount', 'budgetstatus', 'deliverymethod',
         'isbudgetexplicitlyshared', 'period']
}
cols_types = {
    'campaign-basic-stats':
        ['int64', 'int64', 'int64', 'int64', 'int64', 'float64', 'int64',
         'int64', 'float64', 'str', 'str', 'float64', 'int64', 'float64',
         'float64', 'int64', 'str', 'str', 'int64', 'str', 'int64',
         'str', 'int64'],
    'budget-stats':
        ['int64', 'int64', 'int64', 'float64', 'float64', 'float64', 'str',
         'str', 'int64', 'int64', 'float64', 'int64', 'float64', 'float64',
         'str', 'int64', 'float64', 'float64', 'float64', 'int64', 'int64',
         'int64', 'float64', 'float64', 'float64', 'int64', 'int64', 'float64',
         'str', 'int64', 'float64', 'float64', 'float64', 'int64', 'int64'],
    'budget':
        ['int64', 'int64', 'str', 'int64', 'str', 'int64', 'str', 'str', 'bool',
         'str']
}

# settings for loading and writing the csv file
sep = ','
partition_cols = ['externalcustomerid']

# s3fs.S3FileSystem is used to simplify S3 read/write using pandas and Arrow
s3_fs_dest = s3fs.S3FileSystem(False, access_key, secret)

# set the Parquet destination in S3
destination = {
    'campaign-basic-stats':
        's3://accuen-test/adam_test/adam-adwords-dashboard/campaign-basic-stats',
    'budget-stats':
        's3://accuen-test/adam_test/adam-adwords-dashboard/budget-stats',
    'budget': 's3://accuen-test/adam_test/adam-adwords-dashboard/budget'
}

for table in ['campaign-basic-stats', 'budget-stats', 'budget']:
    # cols_names = pd.read_csv(file_path[table], nrows=0).columns
    types_dict = dict(zip(cols_names[table], cols_types[table]))

    df = pd.read_csv(file_path[table], sep=sep, names=cols_names[table],
                     skiprows=[0], dtype=types_dict)
    pq.write_to_dataset(pa.Table.from_pandas(df), destination[table],
                        filesystem=s3_fs_dest,
                        partition_cols=partition_cols,
                        compression='gzip')
    # gc.collect due to https://github.com/apache/arrow/issues/1285
    gc.collect()
