import pandas as pd

access_key = input('Enter AWS key:')
secret = input('Enter AWS secret:')

file_path = {
    'basic-stats':
        'tables_test/adwords-tables-test_campaign-basic-stats.csv',
    'budget-stats':
        'tables_test/adwords-tables-test_budget-stats.csv',
    'budget': 'tables_test/adwords-tables-test_budget.csv'
}

# types of columns from csv file
# assuming that no values have been nulled
cols_types = {
    'basic-stats':
        ['int64', 'int64', 'int64', 'int64', 'int64', 'float64', 'int64',
         'int64', 'float64', 'str', 'str', 'float64', 'int64', 'float64',
         'float64', 'int64', 'str', 'str', 'int64', 'str', 'int64',
         'str', 'int64'],
    'budget-stats':
        ['int64', 'int64', 'int64', 'float64', 'float64', 'float64', 'str',
         'str', 'int64', 'int64', 'float64', 'int64', 'float64', 'float64',
         'str', 'int64', 'float64', 'float64', 'float64', 'int64', 'int64',
         'int64', 'float64', 'float64', 'float64', 'int64', 'int64', 'float64',
         'str', 'int64', 'float64', 'float64', 'float64', 'int64', 'int64'],
    'budget':
        ['int64', 'int64', 'str', 'int64', 'str', 'int64', 'str', 'str', 'bool',
         'str']
}

# settings for loading and writing the csv file
sep = ','

for table in ['basic-stats', 'budget-stats', 'budget']:
    cols_names = pd.read_csv(file_path[table], nrows=0).columns
    types_dict = dict(zip(cols_names, cols_types[table]))
    print(types_dict)